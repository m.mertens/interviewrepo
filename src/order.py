from enum import StrEnum
from typing import List

class OrderStatus(StrEnum):
    PENDING = "PENDING"
    PROCESSING = "PROCESSING"
    COMPLETED = "COMPLETED"



class Order:
    order_id: str
    name: str
    items: List[str]
    status: OrderStatus

    def __init__(self, order_id: str, name: str, items: List[str]) -> None:
        self.order_id = order_id
        self.name = name
        self.items = items
        self.status = OrderStatus.PENDING

    def process_order(self):
        print(f"Order {self.order_id} is processing")
        self.status = OrderStatus.PROCESSING

    def complete_order(self):
        print(f"Order {self.order_id} is completed")
        self.status = OrderStatus.COMPLETED


class OrderQueue:
    orders: List[Order]

    def __init__(self) -> None:
        self.orders = []


    def add_order(self, order: Order) -> None:
        self.orders.append(order)

    def process_order(self, order: Order) -> None:
        order = self.orders.pop()
        order.process_order()
        order.complete_order()

    def get_pending_orders(self) -> List[Order]:
        return [order for order in self.orders if order.status == OrderStatus.PENDING]
