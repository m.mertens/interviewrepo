import pytest
from src.order import Order, OrderQueue, OrderStatus


def test_that_a_pending_order_can_be_processed():
    order = Order(order_id="123", name="Someone", items=["item 1", "item 2"])
    order.process_order()
    assert order.status == OrderStatus.PROCESSING

def test_that_a_processing_order_can_be_processed():
    order = Order(order_id="123", name="Someone", items=["item 1", "item 2"])
    order.process_order()
    order.complete_order()
    assert order.status == OrderStatus.COMPLETED

def test_that_a_pending_order_cannot_be_completed():
    order = Order(order_id="123", name="Someone", items=["item 1", "item 2"])
    with pytest.raises(ValueError):
        order.complete_order()

def test_that_a_completed_order_cannot_be_processed():
    order = Order(order_id="123", name="Someone", items=["item 1", "item 2"])
    order.process_order()
    order.complete_order()
    with pytest.raises(ValueError):
        order.process_order()



def test_that_a_order_can_be_added():
    order = Order(order_id="123", name="Someone", items=["item 1", "item 2"])
    queue = OrderQueue()
    queue.add_order(order)


def test_that_only_pending_order_can_be_added():
    order = Order(order_id="123", name="Someone", items=["item 1", "item 2"])
    order.process_order()
    queue = OrderQueue()
    with pytest.raises(ValueError):
        queue.add_order(order)

def test_that_only_orders_can_be_added_to_the_queue():
    queue = OrderQueue()
    with pytest.raises(TypeError):
        queue.add_order("")

